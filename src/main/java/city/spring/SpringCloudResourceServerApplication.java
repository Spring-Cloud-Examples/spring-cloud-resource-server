package city.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 资源服务应用
 *
 * @author HouKunLin
 * @date 2019/12/5 0005 16:26
 */
@SpringBootApplication
public class SpringCloudResourceServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudResourceServerApplication.class, args);
    }

}
