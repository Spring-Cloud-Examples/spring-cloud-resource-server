package city.spring.configure.resource;

import city.spring.configure.security.CustomAuthenticationEntryPoint;
import city.spring.configure.security.CustomDefaultAuthorizeRequestsCustomizer;
import city.spring.configure.security.handler.CustomAccessDeniedHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

import javax.annotation.PostConstruct;

/**
 * 资源配置服务
 *
 * @author HouKunLin
 * @date 2019/12/3 0003 22:23
 */
@Configuration
@EnableResourceServer
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class CustomResourceServerConfiguration extends ResourceServerConfigurerAdapter {
    private final static Logger logger = LoggerFactory.getLogger(CustomResourceServerConfiguration.class);
    @Autowired
    private CustomAccessDeniedHandler accessDeniedHandler;
    @Autowired
    private CustomAuthenticationEntryPoint authenticationEntryPoint;
    @Autowired
    private ResourceServerProperties resourceServerProperties;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.accessDeniedHandler(accessDeniedHandler);
        resources.authenticationEntryPoint(authenticationEntryPoint);
        // 设置当前应用的资源ID，该ID必须包含在客户端的resourceIds列表中，否则这个客户端分发的Token不能访问当前服务
        resources.resourceId(resourceServerProperties.getResourceId());
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.cors().disable();
        http.exceptionHandling()
                .accessDeniedHandler(accessDeniedHandler)
                .authenticationEntryPoint(authenticationEntryPoint)
        ;
        http.authorizeRequests(new CustomDefaultAuthorizeRequestsCustomizer());
    }

    @PostConstruct
    public void postConstruct() {
        logger.debug("自定义资源服务器配置: {}", this);
    }
}
