package city.spring.configure;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoRestTemplateFactory;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

/**
 * 应用的Bean配置
 *
 * @author HouKunLin
 * @date 2019/12/8 0008 23:37
 */
@Configuration
public class ApplicationBeanConfiguration {
    private final static Logger logger = LoggerFactory.getLogger(ApplicationBeanConfiguration.class);
    @Autowired(required = false)
    private RemoteTokenServices remoteTokenServices;

    /**
     * 引入OAuth2后无法使用负载均衡，使用该方法添加负载均衡能力。
     * 主要解决问题是微服务无法与身份认证服务进行通信（使用服务名称service-auth），会报无法找到service-auth这个主机信息
     *
     * @param factory factory
     * @return OAuth2请求工具
     */
    @LoadBalanced
    @Bean
    public OAuth2RestTemplate oAuth2RestTemplate(UserInfoRestTemplateFactory factory) {
        return factory.getUserInfoRestTemplate();
    }

    @LoadBalanced
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @PostConstruct
    public void postConstruct() {
        if (remoteTokenServices != null) {
            // 在使用 /oauth/check_token 接口校验Token是否有效时，为 token-info-uri 提供负载均衡功能，使host可以设置为微服务名来进行访问
            remoteTokenServices.setRestTemplate(restTemplate());
        }
    }
}
