package city.spring.configure.security;

import city.spring.configure.resource.OAuth2RequestedMatcher;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

/**
 * 自定义授权请求自定义程序。
 * 默认所有的链接都开放，任何人可以访问，
 * 但是当携带Token时，这个链接就为必须登录才能授权（也就是这个Token必须有效），否则访问失败
 *
 * @author HouKunLin
 * @date 2019/12/1 0001 22:18
 */
public class CustomTokenAuthorizeRequestsCustomizer implements Customizer<ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry> {
    @Override
    public void customize(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry expressionInterceptUrlRegistry) {
        expressionInterceptUrlRegistry.requestMatchers(EndpointRequest.toAnyEndpoint()).permitAll();
        // 决定哪些请求经过资源服务认证处理（结果为true时），当结果为false时不经过资源程序处理，直接可以访问
        expressionInterceptUrlRegistry.requestMatchers(new OAuth2RequestedMatcher()).authenticated();
        expressionInterceptUrlRegistry.anyRequest().permitAll();
    }
}
