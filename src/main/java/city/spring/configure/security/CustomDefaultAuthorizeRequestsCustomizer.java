package city.spring.configure.security;

import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

/**
 * 自定义授权请求自定义程序。
 * 默认只开放 /test/** 路径，任何人都可以访问，其他路径则必须登录后才能访问。
 * 当携带Token 访问 /test/** 时可以获取到Token对应的用户信息，如果Token失效，则无法获取用户信息，但是请求仍旧有效
 *
 * @author HouKunLin
 * @date 2019/12/1 0001 22:18
 */
public class CustomDefaultAuthorizeRequestsCustomizer implements Customizer<ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry> {
    @Override
    public void customize(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry expressionInterceptUrlRegistry) {
        expressionInterceptUrlRegistry.requestMatchers(EndpointRequest.toAnyEndpoint()).permitAll();
        expressionInterceptUrlRegistry.antMatchers("/test/**").permitAll();
        expressionInterceptUrlRegistry.anyRequest().authenticated();
    }
}
