package city.spring.configure.security.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义访问拒绝处理程序
 *
 * @author HouKunLin
 * @date 2019/12/1 0001 21:54
 */
@Component
public class CustomAccessDeniedHandler implements AccessDeniedHandler {
    private final static Logger logger = LoggerFactory.getLogger(CustomAccessDeniedHandler.class);
    private final AccessDeniedHandler accessDeniedHandler = new OAuth2AccessDeniedHandler();

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {
        logger.error("自定义访问拒绝处理程序", e);
        accessDeniedHandler.handle(request, response, e);
    }

    @PostConstruct
    public void postConstruct() {
        logger.debug("自定义访问拒绝处理程序: {}", this);
    }
}
