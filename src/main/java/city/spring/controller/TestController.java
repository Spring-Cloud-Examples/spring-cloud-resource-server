package city.spring.controller;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.*;

/**
 * 首页
 *
 * @author HouKunLin
 * @date 2019/12/3 0003 21:53
 */
@Slf4j
@Data
@RestController
@RequestMapping
public class TestController {
    private ApplicationContext applicationContext;

    @Autowired
    public TestController(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @RequestMapping("/test/test")
    public Object indexTest(Principal user) {
        return timeZone(user);
    }

    @RequestMapping("/index")
    public Object index(Principal user) {
        return timeZone(user);
    }

    private Map<String, Object> timeZone(Principal user) {
        val timeZone = TimeZone.getDefault();
        HashMap<String, Object> map = new LinkedHashMap<>(16);
        map.put("ID", timeZone.getID());
        map.put("displayName", timeZone.getDisplayName());
        map.put("zoneId", timeZone.toZoneId());
        map.put("DSTSavings", timeZone.getDSTSavings());
        map.put("useDaylightTime", timeZone.useDaylightTime());
        map.put("observesDaylightTime", timeZone.observesDaylightTime());
        map.put("applicationId", applicationContext.getId());
        map.put("applicationName", applicationContext.getApplicationName());
        map.put("applicationDisplayName", applicationContext.getDisplayName());
        map.put("applicationBeanDefinitionCount", applicationContext.getBeanDefinitionCount());
        map.put("timestamp", new Date());
        map.put("user", user);
        return map;
    }
}
